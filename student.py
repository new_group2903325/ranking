def parse_line(line : str) -> list:
    name, *marks = line.strip().split(" ")
    mark1 = [int(_) for _ in marks[:1]]
    mark2 = [int(_) for _ in marks[1:2]]
    mark3 = [int(_) for _ in marks[2:3]]
    mark4 = [int(_) for _ in marks[3:]]
    return (name, mark1, mark2, mark3, mark4) 

def load_data(mark_file : str) -> list:  ##mark_file is the data file
    return [parse_line(line) for line in open(mark_file)]


class student:
  def __init__(self, list_score):
    self.list_score= []

# let's define > as 1, < as 2 and # as 3
def compare(a : student, b : student) -> int :
    i = 0
    for i in range(0,len(a.list_score)) :
        if(a.list_score[i] > b.list_score[i]):
            i += 1

    if i == len(a.list_score) :
        return 1
    elif i == 0 :
        return 2
    else :
        return 3

lst = load_data("\\wsl.localhost\Ubuntu\home\sara\ranking\students.txt")
for i in lst :
    print(parse_line(i))

